package lab01.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList{

    private final List<Integer> list = new ArrayList<>();
    private int index = 0;

    @Override
    public void add(int element) {
        this.list.add(element);
    }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        return this.selectElement(this.index++);
    }

    @Override
    public Optional<Integer> previous() {
        return this.selectElement(this.index--);
    }

    @Override
    public void reset() {
        this.index = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        int temp = index;
        Optional<Integer> element;
        do{
            element = next();
            if (element.isPresent() && strategy.apply(element.get()))
                return element;
            else
                element = Optional.empty();
        } while(temp != index);
        return element;
    }

    private Optional<Integer> selectElement(int in){
        Optional<Integer> element = Optional.of(this.list.get(in));
        correctIndex();
        return element;
    }

    private void correctIndex(){
        if (this.index == this.list.size())
            this.index = 0;
        else if (this.index == -1)
            this.index = this.list.size() - 1;
    }
}
