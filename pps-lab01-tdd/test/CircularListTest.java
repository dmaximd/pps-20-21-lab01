import lab01.tdd.CircularList;
import lab01.tdd.CircularListImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    public static final List<Integer> ELEMENT_ARRAY = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
    public static final int CYCLES = 2;
    public static final int INTERMEDIATE_POSITION = 5;
    public static final int ELEMENT_NOT_IN_LIST = 20;
    private CircularList circularList;

    @BeforeEach
    void beforeEach(){
        circularList = new CircularListImpl();
    }

    @Test
    void testListIitiallyEmpty(){
        assertTrue(this.circularList.isEmpty());
    }

    @Test
    void testAddElement(){
        this.circularList.add(ELEMENT_ARRAY.get(0));
        assertEquals(1, this.circularList.size());
    }

    @Test
    void testMultipleElementAdd(){
        addAllElements();
        assertEquals(ELEMENT_ARRAY.size(), this.circularList.size());
    }

    @Test
    void testNextElements(){
        addAllElements();
        for (Integer element: ELEMENT_ARRAY)
            assertEquals(Optional.of(element), this.circularList.next());
    }

    @Test
    void testNextCicrcularity(){
        addAllElements();
        for (int repetition = 0; repetition < CYCLES; repetition++)
            for (Integer element: ELEMENT_ARRAY)
                assertEquals(Optional.of(element), this.circularList.next());
    }

    @Test
    void testPreviousElements(){
        addAllElements();
        for (int elementIndex = 0; elementIndex < ELEMENT_ARRAY.size() - 1; elementIndex++)
            this.circularList.next();
        for (int elementIndex = ELEMENT_ARRAY.size() - 1; elementIndex >= 0; elementIndex--)
            assertEquals(Optional.of(ELEMENT_ARRAY.get(elementIndex)), this.circularList.previous());
    }

    @Test
    void testPreviousCircularity(){
        addAllElements();
        this.circularList.previous();
        for (int repetition = 0; repetition < CYCLES; repetition++)
            for (int elementIndex = ELEMENT_ARRAY.size() - 1; elementIndex >= 0; elementIndex--)
                assertEquals(Optional.of(ELEMENT_ARRAY.get(elementIndex)), this.circularList.previous());
    }

    @Test
    void testReset(){
        addAllElements();
        for (int index = 0; index < INTERMEDIATE_POSITION; index++)
            this.circularList.next();
        assertEquals(Optional.of(ELEMENT_ARRAY.get(INTERMEDIATE_POSITION)), this.circularList.next());
        this.circularList.reset();
        assertEquals(Optional.of(ELEMENT_ARRAY.get(0)), this.circularList.next());
    }

    @Test
    void testStrategyEven(){
        addAllElements();
        for (Integer element: ELEMENT_ARRAY){
            if (element % 2 == 0)
                assertEquals(Optional.of(element), this.circularList.next(el -> el % 2 == 0));
        }
    }

    @Test
    void testStrategyElementNotInList(){
        addAllElements();
        assertEquals(Optional.empty(), this.circularList.next(el -> el == ELEMENT_NOT_IN_LIST));
    }

    private void addAllElements(){
        for (Integer element: ELEMENT_ARRAY)
            this.circularList.add(element);
    }
}
