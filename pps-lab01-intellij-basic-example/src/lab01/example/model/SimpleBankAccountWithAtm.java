package lab01.example.model;

/**
 * This class represent a particular instance of a BankAccount.
 * In particular, a Simple Bank Account allows always the deposit
 * while the withdraw is allowed only if the balance greater or equal the withdrawal amount
 * There are additionally the deposit and withdraw operations that can be performed on an ATM
 * with a little amount of fee that is scaled with each ATM transaction
 */
public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {

    public static final int TRANSACTION_FEE = 1;

    public SimpleBankAccountWithAtm(AccountHolder accountHolder, double initialAmount) {
        super(accountHolder, initialAmount);
    }

    @Override
    public void depositWithAtm(int usrID, double amount) {
        this.deposit(1, depositWithFee(amount));
    }

    @Override
    public void withdrawWithAtm(int usrID, double amount) {
        if (checkWithdrawableWithAtm(amount))
            this.withdraw(1, withdrawWithFee(amount));
    }

    private boolean checkWithdrawableWithAtm(double amount){
        return this.getBalance() > (amount + TRANSACTION_FEE);
    }

    private double depositWithFee(double amount){
        return amount - TRANSACTION_FEE;
    }

    private double withdrawWithFee(double amount){
        return amount + TRANSACTION_FEE;
    }
}
