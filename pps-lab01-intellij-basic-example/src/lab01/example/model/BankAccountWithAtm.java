package lab01.example.model;

public interface BankAccountWithAtm extends BankAccount{

    /**
     * Allows the deposit of an amount on the account, if the given usrID corresponds to the register holder ID
     * of the bank account. This ID acts like an "identification token" . This deposit applies an additional
     * fee for the ATM transaction.
     *
     * @param userID the id of the user that wants do the deposit
     * @param amount the amount of the deposit
     */
    void depositWithAtm(int userID, double amount);

    /**
     * Allows the withdraw of an amount from the account, if the given usrID corresponds to the register holder ID
     * of the bank account. This ID acts like an "identification token" . This withdraw applies an additional
     * fee for the ATM transaction.
     *
     * @param userID the id of the user that wants do the withdraw
     * @param amount the amount of the withdraw
     */
    void withdrawWithAtm(int userID, double amount);
}
