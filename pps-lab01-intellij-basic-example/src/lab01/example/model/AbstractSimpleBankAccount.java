package lab01.example.model;

/**
 * This class represent an abstract instance of a BankAccount.
 * In particular, the abstract class tracks the user that this particular BankAccount links to
 * and checks that the user that tries to perform any operation is the holder of the BankAccount
 */
public abstract class AbstractSimpleBankAccount implements BankAccount{

    private final AccountHolder accountHolder;

    public abstract double getBalance();

    public abstract void deposit(final int usrID, final double amount);

    public abstract void withdraw(final int usrID, final double amount);

    AbstractSimpleBankAccount(AccountHolder accountHolder){
        this.accountHolder = accountHolder;
    }

    public AccountHolder getHolder(){
        return this.accountHolder;
    }

    protected boolean checkUser(final int id){
        return this.accountHolder.getId() == id;
    }

}
