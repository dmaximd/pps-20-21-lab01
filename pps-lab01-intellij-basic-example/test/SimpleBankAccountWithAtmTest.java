import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import lab01.example.model.BankAccountWithAtm;
import lab01.example.model.SimpleBankAccountWithAtm;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class SimpleBankAccountWithAtmTest extends AbstractSimpleBankAccountTest{

    public static final double DEPOSIT_AMOUNT = 100;
    public static final double WITHDRAW_AMOUNT = 30;
    public static final int TRANSACTION_FEE = 1;
    private BankAccountWithAtm bankAccount;

    @Override
    void initBankAccount(AccountHolder accountHolder, double initialAmount) {
        this.bankAccount = new SimpleBankAccountWithAtm(accountHolder, initialAmount);
    }

    @Override
    BankAccount getBankAccount() {
        return this.bankAccount;
    }

    @Test
    void testDepositWithAtm(){
        this.bankAccount.depositWithAtm(1, DEPOSIT_AMOUNT);
        assertEquals(DEPOSIT_AMOUNT - TRANSACTION_FEE, this.bankAccount.getBalance());
    }

    @Test
    void testWithdrawWithAtm(){
        this.bankAccount.deposit(1, DEPOSIT_AMOUNT);
        this.bankAccount.withdrawWithAtm(1, WITHDRAW_AMOUNT);
        assertEquals(DEPOSIT_AMOUNT - WITHDRAW_AMOUNT - TRANSACTION_FEE, this.bankAccount.getBalance());
    }

    @Test
    void testWithdrawWrongAmmountWithFee(){
        this.bankAccount.deposit(1, DEPOSIT_AMOUNT);
        this.bankAccount.withdrawWithAtm(1, DEPOSIT_AMOUNT);
        assertEquals(DEPOSIT_AMOUNT, this.bankAccount.getBalance());
    }
}
