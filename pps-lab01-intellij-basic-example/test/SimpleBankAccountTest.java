import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import lab01.example.model.SimpleBankAccount;

/**
 * The test suite for testing the SimpleBankAccount implementation
 */
class SimpleBankAccountTest extends AbstractSimpleBankAccountTest{

    private BankAccount bankAccount;

    @Override
    void initBankAccount(AccountHolder accountHolder, double initialAmount) {
        this.bankAccount = new SimpleBankAccount(accountHolder, initialAmount);
    }

    @Override
    BankAccount getBankAccount() {
        return this.bankAccount;
    }
}
